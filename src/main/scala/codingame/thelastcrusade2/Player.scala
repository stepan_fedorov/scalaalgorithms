package codingame.thelastcrusade2

import scala.io.StdIn._

/**
  * Auto-generated code below aims at helping you parse
  * the standard input according to the problem statement.
  **/
object Player extends App {
  // w: number of columns.
  // h: number of rows.
  val Array(w, h) = for (i <- readLine split " ") yield i.toInt
  for (i <- 0 until h) {
    val line = readLine // each line represents a line in the grid and contains W integers T. The absolute value of T specifies the type of the room. If T is negative, the room cannot be rotated.
    Console.err.println(line)
  }
  val ex = readInt // the coordinate along the X axis of the exit.

  // game loop
  while (true) {
    val Array(_xi, _yi, posi) = readLine split " "
    val xi = _xi.toInt
    val yi = _yi.toInt
    val r = readInt // the number of rocks currently in the grid.
    for (i <- 0 until r) {
      val Array(_xr, _yr, posr) = readLine split " "
      val xr = _xr.toInt
      val yr = _yr.toInt
    }

    // Write an action using println
    // To debug: Console.err.println("Debug messages...")


    // One line containing on of three commands: 'X Y LEFT', 'X Y RIGHT' or 'WAIT'
    println("WAIT")
  }
}

object Directions {

  sealed abstract class Direction(val x: Int, val y: Int)

  case object Up extends Direction(0, -1)

  case object Down extends Direction(0, 1)

  case object Left extends Direction(-1, 0)

  case object Right extends Direction(1, 0)

}

import codingame.thelastcrusade2.Directions._

case class Link(in: Direction, out: Direction)

case class Type(idx: Int, left: Int, right: Int, links: List[Link])

object Types {
  private var types = List(
    Type(0, 0, 0, List.empty[Link]),
    Type(1, 1, 1, List(Link(Up, Down), Link(Left, Down), Link(Right, Down))),
    Type(2, 3, 3, List(Link(Left, Right), Link(Right, Left))),
    Type(3, 2, 2, List(Link(Up, Down))),
    Type(4, 5, 5, List(Link(Up, Left), Link(Right, Down))),
    Type(5, 4, 4, List(Link(Up, Right), Link(Left, Down))),
    Type(6, 9, 7, List(Link(Left, Right), Link(Right, Left))),
    Type(7, 6, 8, List(Link(Up, Down), Link(Right, Down))),
    Type(8, 7, 9, List(Link(Left, Down), Link(Right, Down))),
    Type(9, 8, 6, List(Link(Up, Down), Link(Left, Down))),
    Type(10, 13, 11, List(Link(Up, Left))),
    Type(11, 10, 12, List(Link(Up, Right))),
    Type(12, 11, 13, List(Link(Right, Down))),
    Type(13, 12, 10, List(Link(Left, Down)))
  )




}
