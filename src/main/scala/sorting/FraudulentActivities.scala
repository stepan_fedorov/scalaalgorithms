package sorting

import scala.io.StdIn._

object FraudulentActivities extends App {

  val Array(n, d) = for (e <- readLine split " ") yield e.toInt
  val a = for (e <- readLine split " ") yield e.toInt
  val expenditure = Array.ofDim[Int](201)

  for (i <- 0 until d) {
    expenditure(a(i)) += 1
  }

  def doubleMedian(): Int = {
    val i1 = d / 2

    var idx = 0
    var element = expenditure(0)
    for (i <- 0 until 201) {
      val current = expenditure(i)
      if (current > 0) {
        if (current + idx > i1) {
          if (idx < i1 || d % 2 == 1) {
            return i * 2
          } else {
            return element + i
          }
        }
        idx += current
        element = i
      }
    }
    0
  }

  var res = 0

  var dm = doubleMedian()
  for (i <- d until n) {
    res += (if (a(i) >= dm) 1 else 0)
    expenditure(a(i - d)) -= 1
    expenditure(a(i)) += 1
    dm = doubleMedian()
  }

  println(res)

}
