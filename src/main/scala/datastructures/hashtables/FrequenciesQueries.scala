package datastructures.hashtables

import java.io.{FileInputStream, PrintWriter}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.StdIn
import scala.io.StdIn._
import scala.math._

object FrequenciesQueries extends App {

  case class Counter(var size: Int) {
    def inc(): Unit = {
      size += 1
    }

    def dec(): Unit = {
      size = max(0, size - 1)
    }
  }

  val idFreq = mutable.LongMap.empty[Counter]
  val freqCount = Array.fill[Int](1000000)(0)
  val results = ArrayBuffer.empty[Int]

  val printWriter = new PrintWriter(sys.env("OUTPUT_PATH"))

  val q = StdIn.readLine.trim.toInt

  val queries = Array.ofDim[Int](q, 2)

  for (i <- 0 until q) {
    queries(i) = StdIn.readLine.replaceAll("\\s+$", "").split(" ").map(_.trim.toInt)
  }

  for (Array(action, id) <- queries) {
    val counter = idFreq.getOrElseUpdate(id, Counter(0))
    val prev = counter.size
    action match {
      case 1 =>
        counter.inc()
        updateFrequencyCount(prev, counter.size)
      case 2 =>
        counter.dec()
        updateFrequencyCount(prev, counter.size)
      case 3 =>
        val res = if (id < 1000000 && freqCount(id) > 0) 1 else 0
        results.append(res)
    }
  }

  printWriter.println(results.mkString("\n"))
  printWriter.close()

  @inline private def updateFrequencyCount(prev: Int, current: Int): Unit = {
    if (prev >= 0) {
      freqCount(prev) = max(0, freqCount(prev) - 1)
    }
    if (current >= 0) {
      freqCount(current) = freqCount(current) + 1
    }
  }
}
