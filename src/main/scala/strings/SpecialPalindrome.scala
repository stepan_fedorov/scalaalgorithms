package strings

import scala.io.StdIn._

object SpecialPalindrome extends App {

  val n = readInt
  val s = readLine.toList

  def countWithHead(prev: List[Char], s: List[Char]): Int = {
    var counter = 0
    if (s.nonEmpty) {
      if (prev.head == s.head) {
        counter += 1
        counter += countWithHead(s.head :: prev, s.tail)
      }
      else {
        var t1 = prev
        var t2 = s.tail
        while (t1.nonEmpty && t2.nonEmpty && t1.head == t2.head) {
          t1 = t1.tail
          t2 = t2.tail
        }
        if (t1.isEmpty) counter += 1
      }
    }
    counter
  }

  def count(prev: List[Char], s: List[Char]): Int = {
    var counter = if (prev.nonEmpty) 1 else 0
    if (s.nonEmpty) {
      if (prev.nonEmpty) {
        counter += countWithHead(prev, s)
      }
      counter += count(s.head :: Nil, s.tail)
    }
    counter
  }

  println(count(List.empty[Char], s))
}
