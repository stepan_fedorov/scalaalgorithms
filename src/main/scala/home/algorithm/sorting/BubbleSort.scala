package home.algorithm.sorting

/**
  * Created by fedor on 6/17/2017.
  */
object BubbleSort extends App {

  private def swap[T](a: Array[T], i: Int, j: Int): Unit = {
    val v = a(i)
    a(i) = a(j)
    a(j) = v
  }

  def directSort[T](a: Array[T])(implicit ord: Ordering[T]): Unit = {
    var n = a.length
    while (n > 0) {
      var end = 0
      for (i <- 1 until n) {
        if (ord.gt(a(i - 1), a(i))) {
          swap(a, i - 1, i)
          end = i
        }
      }
      n = end
    }
  }

  def directRecursiveSort[T](a: Array[T])(implicit ord: Ordering[T]): Unit = {
    directRecursiveSort(a, a.length)
  }

  private def directRecursiveSort[T](a: Array[T], n: Int)(implicit ord: Ordering[T]): Unit = {
    var end = 0
    for (i <- 1 until n) {
      if (ord.gt(a(i - 1), a(i))) {
        swap(a, i - 1, i)
        end = i
      }
    }
    if (end > 1) {
      directRecursiveSort(a, end)
    }
  }

  def listSort[T](l: List[T])(implicit ord: Ordering[T]): List[T] = {
    if (l.size > 1) {
      val ol = (List[T](l.head) /: l.tail)((r, e) => {
        if (ord.lt(e, r.head))
          e :: r
        else
          r.head :: e :: r.tail
      })
      ol.head :: listSort(ol.tail)
    } else {
      l
    }
  }

}
