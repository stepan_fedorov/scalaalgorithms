package home.algorithm.sorting

/**
  * Created by fedor on 6/18/2017.
  */
object InsertionSort {

  private def swap[T](a: Array[T], i: Int, j: Int): Unit = {
    val v = a(i)
    a(i) = a(j)
    a(j) = v
  }

  def directSort[T](a: Array[T])(implicit ord: Ordering[T]): Unit = {
    for (i <- 1 until a.length) {
      val x = a(i)
      var j = i - 1
      while (j >= 0 && ord.gt(a(j), x)) {
        a(j + 1) = a(j)
        j -= 1
      }
      a(j + 1) = x
    }
  }

  def directRecursiveSort[T](a: Array[T])(implicit ord: Ordering[T]): Unit = {
    directRecursiveSort(a, a.length - 1)
  }

  private def directRecursiveSort[T](a: Array[T], i: Int)(implicit ord: Ordering[T]): Unit = {
    if (i > 1) {
      directRecursiveSort(a, i - 1)
      val x = a(i)
      var j = i - 1
      while (j >= 0 && ord.gt(a(j), x)) {
        a(j + 1) = a(j)
        j -= 1
      }
      a(j + 1) = x
    }
  }

  def listSort[T](a: List[T])(implicit ord: Ordering[T]): List[T] = {
    def insert(b: List[T], value: T) = {
      val (head, tail) = b.span(ord.lt(_, value))
      head ::: (value :: tail)
    }

    (List[T]() /: a) ((l, e) => insert(l, e))
  }

}
