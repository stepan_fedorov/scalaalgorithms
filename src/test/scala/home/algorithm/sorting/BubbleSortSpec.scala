package home.algorithm.sorting

import org.scalatest._

/**
  * Created by fedor on 6/17/2017.
  */
class BubbleSortSpec extends FlatSpec with Matchers {

  "BubbleSort directSort" should "should sort elements in ascending order" in {
    val array = Array(7, 8, 6, 9, 5, 1, 2, 3, 4)
    BubbleSort.directSort(array)
    array should be {
      Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

  "BubbleSort directRecursiveSort" should "should sort elements in ascending order" in {
    val array = Array(7, 8, 6, 9, 5, 1, 2, 3, 4)
    BubbleSort.directRecursiveSort(array)
    array should be {
      Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

  "BubbleSort listSort" should "should sort elements in ascending order" in {
    val list = List(7, 8, 6, 9, 5, 1, 2, 3, 4)
    val sortedList = BubbleSort.listSort(list)
    sortedList should be {
      List(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

}
