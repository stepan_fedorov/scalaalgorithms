package home.algorithm.sorting

import org.scalatest._

/**
  * Created by fedor on 6/17/2017.
  */
class InsertionSortSpec extends FlatSpec with Matchers {

  "InsertionSort directSort" should "should sort elements in ascending order" in {
    val array = Array(7, 8, 6, 9, 5, 1, 2, 3, 4)
    InsertionSort.directSort(array)
    array should be {
      Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

  "InsertionSort directRecursiveSort" should "should sort elements in ascending order" in {
    val array = Array(7, 8, 6, 9, 5, 1, 2, 3, 4)
    InsertionSort.directRecursiveSort(array)
    array should be {
      Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

  "InsertionSort listSort" should "should sort elements in ascending order" in {
    val list = List(7, 8, 6, 9, 5, 1, 2, 3, 4)
    val sortedList = InsertionSort.listSort(list)
    sortedList should be {
      List(1, 2, 3, 4, 5, 6, 7, 8, 9)
    }
  }

}
